<?php 

    include "./base.php";
    include "./models/paper.php";

    if($method == "GET") {
        header("Content-Type: application/json");

        $stmt = $db->prepare("SELECT * FROM paper WHERE id = ?");
        $stmt->execute([$id]);
        
        $results = $stmt->fetchAll(PDO::FETCH_CLASS, "Paper");
        
        if($results) {
            echo json_encode($results[0]);
        } else {
            http_response_code(404);
        }
    }

    else if($method == "POST") {
        $content = trim(file_get_contents("php://input"));
        $paper = json_decode($content);

        if($paper->studentId && $paper->filename) {
            $stmt = $db->prepare("INSERT INTO paper (studentId, filename) VALUES(?, ?)");
            $res = $stmt->execute([$paper->studentId, $paper->filename]);
            
            if($res) {
                header("Content-Type: application/json");
                
                $result = new stdClass();
                $result->id = $db->lastInsertId();
                
                echo json_encode($result);
            } else {
                http_response_code(400);
            }
        }
        else {
            http_response_code(400);
        }
    }

    else if($method == "PUT") {
        $content = trim(file_get_contents("php://input"));
        $paper = json_decode($content);

        if($id && $paper->studentId && $paper->filename) {
            $stmt = $db->prepare("UPDATE paper SET studentId=?, filename=? WHERE id=?");
            $res = $stmt->execute([$paper->studentId, $paper->filename, $id]);
            
            http_response_code($res ? 200 : 400);
        } else {
            http_response_code(400);
        }
    }

    else if($method == "DELETE") {
        $stmt = $db->prepare("DELETE FROM paper WHERE id = ?");
        $stmt->execute([$id]);
    }

    else {
        // method not allowed
        http_response_code(405);
    }
?>