<?php

    // include database
    include_once './config/database.php';
 
    // instantiate database
    $db = (new Database())->getConnection();
 
    // Request method
    $method = $_SERVER['REQUEST_METHOD'];

    // Item id
    $id = $_GET['id'];
?>