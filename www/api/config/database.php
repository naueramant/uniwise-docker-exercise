<?php
class Database{

    private $host = "db";
    private $db_name = "rest";
    private $username = "root";
    private $password = "123123";

    public $conn;
 
    public function getConnection() {
 
        $this->conn = null;
 
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name . ";charset=utf8", $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}
?>