<?php

    include "./base.php";
    include "./models/student.php";

    if($method == "GET") {
        header("Content-Type: application/json");

        $stmt = $db->prepare("SELECT * FROM student WHERE id = ?");
        $stmt->execute([$id]);
        
        $results = $stmt->fetchAll(PDO::FETCH_CLASS, "Student");
        
        if($results) {
            echo json_encode($results[0]);
        } else {
            http_response_code(404);
        }
    }

    else if($method == "POST") {
        $content = trim(file_get_contents("php://input"));
        $student = json_decode($content);

        if($student->name && $student->email && $student->phone) {
            $stmt = $db->prepare("INSERT INTO student (name, email, phone) VALUES(?, ?, ?)");
            $res = $stmt->execute([$student->name, $student->email, $student->phone]);
            
            if($res) {
                header("Content-Type: application/json");
                
                $result = new stdClass();
                $result->id = $db->lastInsertId();
                
                echo json_encode($result);
            } else {
                http_response_code(400);
            }
        }
        else {
            http_response_code(400);
        }
    }

    else if($method == "PUT") {
        $content = trim(file_get_contents("php://input"));
        $student = json_decode($content);

        if($id && $student->name && $student->email && $student->phone) {
            $stmt = $db->prepare("UPDATE student SET name=?, email=?, phone=? WHERE id=?");
            $res = $stmt->execute([$student->name, $student->email, $student->phone, $id]);
            
            http_response_code($res ? 200 : 400);
        } else {
            http_response_code(400);
        }
    }

    else if($method == "DELETE") {
        $stmt = $db->prepare("DELETE FROM student WHERE id = ?");
        $stmt->execute([$id]);
    }

    else {
        // method not allowed
        http_response_code(405);
    }
?>