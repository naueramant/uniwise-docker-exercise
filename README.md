# Simple RESTapi using nginx, php-fpm and mysql

## Run

```bash
docker-compose -f docker-compose.yml up
```

## API test examples

Create a student
```bash
curl -X POST localhost/api/student --data '{"name": "Bob", "email": "bob@gmail.com", "phone": 12345678}'
```

Get a student
```bash
curl -X GET "localhost/api/student?id=1"
```

Change a student
```bash
curl -X PUT "localhost/api/student?id=1" --data '{"name": "Alice", "email": "alice@gmail.com", "phone": 12345678}'
```

Delete a student 
```bash
curl -X DELETE "localhost/api/student?id=1"
```

----

Create a paper
```bash
curl -X POST localhost/api/paper --data '{"studentId": 1, "filename": "handin.pdf"}'
```

Get a paper
```bash
curl -X GET "localhost/api/paper?id=1"
```

Change a paper
```bash
curl -X PUT "localhost/api/paper?id=1" --data '{"studentId": 1, "filename": "handin2.pdf"}'
```

Delete a paper 
```bash
curl -X DELETE "localhost/api/paper?id=1"
```

# Author
Jonas Tranberg Sørensen, April 2018
